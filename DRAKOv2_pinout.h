#ifndef DRAKO_V2_PINOUT
#define DRAKO_V2_PINOUT

#define BUZZER 6
#define Button 12
#define LED 13
#define LDR A6

#define DHT22_PIN 4
#define RGB_LED 16

#define SR04_Trigger 7
#define SR04_Echo 8

#define Left_IR A7
#define Right_IR A3

#define BT_RX 3
#define BT_TX 2

#define Motor_A_IN1 11
#define Motor_A_IN2 5
#define Motor_B_IN3 9
#define Motor_B_IN4 10

#endif
